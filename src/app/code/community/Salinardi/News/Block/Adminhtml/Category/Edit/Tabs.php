<?php
/**
 * Salinardi_News_Block_Adminhtml_Category_Edit_Tabs
 */

/**
 * Class Salinardi_News_Block_Adminhtml_Category_Edit_Tabs
 *
 * Adminhtml Category left tab content for new and edit grid.
 *
 * @category Salinardi
 * @package News
 *
 * @author Manuel Salinardi <salinardii@libero.it>
 * @version 0.2.0
 * @license GNU General Public License, version 3
 */
class Salinardi_News_Block_Adminhtml_Category_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * Salinardi_News_Block_Adminhtml_Category_Edit_Tab constructor.
     *
     * Sets id for the tab.
     * Sets wich form shown in the main content.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('category_edit_tabs');
        $this->setDestElementId('salinardi_news_category_edit_form');
    }

    /**
     * _beforeToHtml
     *
     * Before to render the view creates the tab with its id, label and title.
     *
     * @return Mage_Core_Block_Abstract
     * @throws Exception
     */
    protected function _beforeToHtml()
    {
        try {
            $this->addTab(
                'category_details',
                array(
                    'label' => $this->__('Category Details'),
                    'title' => $this->__('Category Details'),
                )
            );
        } catch (Exception $e) {
            Mage::logException($e);
        }

        return parent::_beforeToHtml();
    }
}
