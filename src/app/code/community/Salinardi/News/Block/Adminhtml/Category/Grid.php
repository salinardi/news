<?php
/**
 * Salinardi_News_Block_Adminhtml_Category_Grid
 */

/**
 * Class Salinardi_News_Block_Adminhtml_Category_Grid
 *
 * Adminhtml Category Grid.
 *
 * @category Salinardi
 * @package News
 *
 * @author Manuel Salinardi <salinardii@libero.it>
 * @version 0.2.0
 * @license GNU General Public License, version 3
 */
class Salinardi_News_Block_Adminhtml_Category_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Salinardi_Pform_Block_Adminhtml_Grid_Grid constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('category_id');  // Grid id
        $this->setDefaultSort('category_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true); // Saves the search results in the sessiom
    }

    /**
     *
     * Fills the table whith the data from Model/Category.php
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    public function _prepareCollection()
    {
        /** @var Varien_Data_Collection $collection */
        $collection = Mage::getModel('salinardi_news/category')->getCollection();

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Grid|Salinardi_News_Block_Adminhtml_Category_Grid
     */
    protected function _prepareColumns()
    {
        // adding columns
        try {
            $this->addColumn(
                'category_id',
                array(
                    'index' => 'category_id',
                    'header' => $this->__('ID'),
                    'width' => 50,
                    'type' => 'number',
                )
            );
            $this->addColumn(
                'code',
                array(
                    'index' => 'code',
                    'header' => $this->__('Code'),
                    'width' => 100,
                    'type' => 'text',
                )
            );
            $this->addColumn(
                'label',
                array(
                    'index' => 'label',
                    'header' => $this->__('Label'),
                    'type' => 'text',
                )
            );

            /** @var Salinardi_News_Model_Source_Status $statusValues */
            $statusValues = Mage::getModel('salinardi_news/source_status');
            $statusValues = $statusValues->toGridArray();

            $this->addColumn(
                'status',
                array(
                    'index' => 'status',
                    'header' => $this->__('Status'),
                    'width' => 50,
                    'type' => 'options',
                    'options' => $statusValues,
                    'renderer' => 'salinardi_news/adminhtml_renderer_status'
                )
            );

            // adding actions to the last column
            $this->addColumn('action',
                array(
                    'header' =>  Mage::helper('salinardi_news')->__('Actions'),
                    'width' => '100',
                    'type' => 'action',
                    'getter' => 'getId', // get id selected row
                    'actions' => array(
                        array(
                            'caption' => $this->__('Edit'),
                            'url' => array('base' => '*/*/edit'),
                            'field' => 'category_id',
                        ),
                        array(
                            'caption' => $this->__('Delete'),
                            'url' => array('base' => '*/*/delete'),
                            'field' => 'category_id',
                            'confirm' => $this->__('Are you sure you want to do this?'),
                        )
                    ),
                    'filter' => false,  //
                    'sortable' => false, // order possibility
                    'index' => 'stores',
                    'is_system' => true, // system function
                )
            );
        } catch (Exception $e) {
            Mage::logException($e);
        }

        // adding mass actions
        $this->addExportType(
            '*/*/exportCsv',
            $this->__('CSV')
        );
        $this->addExportType(
            '*/*/exportXml',
            $this->__('XML')
        );
        return parent::_prepareColumns();
    }

    /**
     * getRowUrl
     *
     * Prepare row URL, linking to edit action.
     *
     * @param Mage_Adminhtml_Block_Widget $item
     * @return string
     * @internal param Varien_Object $row
     */
    public function getRowUrl($item)
    {

        return $this->getUrl('*/*/edit',
                                    array('category_id' => $item->getId()));
    }

}
