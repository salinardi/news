<?php
/**
 * Salinardi_News_Block_Adminhtml_Story
 */

/**
 * Class Salinardi_News_Block_Adminhtml_Story
 *
 * @category Salinardi
 * @package News
 *
 * @author Manuel Salinardi <salinardii@libero.it>
 * @version 0.2.0
 * @license GNU General Public License, version 3
 */
class Salinardi_News_Block_Adminhtml_Story extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'salinardi_news'; // name module
        $this->_controller = 'adminhtml_story'; // path
        $this->_headerText = $this->__('News Story Management');
        parent::__construct();

    }
}
