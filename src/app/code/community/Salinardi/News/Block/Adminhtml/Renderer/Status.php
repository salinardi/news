<?php

/**
 * Salinardi_News_Block_Adminhtml_Renderer_Status
 */

/**
 * Class Salinardi_News_Block_Adminhtml_Renderer_Status
 *
 * Adminhtml Data for fill the select fields.
 *
 * @method string getStatus() magic method
 *
 * @category Salinardi
 * @package News
 *
 * @author Manuel Salinardi <salinardii@libero.it>
 * @version 0.2.0
 * @license GNU General Public License, version 3
 *
 */
class Salinardi_News_Block_Adminhtml_Renderer_Status extends
    Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    /**
     * render
     *
     * Used to render data for a grid column in a fancy way.
     * @param Varien_Object|Salinardi_News_Block_Adminhtml_Renderer_Status|Salinardi_News_Block_Adminhtml_Renderer_Status $row
     * @return string
     *
     */
    public function render(Varien_Object $row)
    {
        if ($row->getStatus() == 0) {
            return '<span class="grid-severity-minor"><span>' . $this->__('Unpublished') . '</span></span>';
        }
        if ($row->getStatus() == 1) {
            return '<span class="grid-severity-notice"><span>' . $this->__('Published') . '</span></span>';
        }
        return '<span class="grid-severity-critical"><span>' . $this->__('Error') . '</span></span>';
    }
}
