<?php
/**
 * Salinardi_News_Block_Adminhtml_Category_Edit_Form
 */

/**
 * Class Salinardi_News_Block_Adminhtml_Category_Edit_Form
 *
 * Adminhtml Category main content for new and edit grid.
 *
 * @category Salinardi
 * @package News
 *
 * @author Manuel Salinardi <salinardii@libero.it>
 * @version 0.2.0
 * @license GNU General Public License, version 3
 */
class Salinardi_News_Block_Adminhtml_Category_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Salinardi_News_Block_Adminhtml_Category_Edit_Form constructor.
     *
     * Sets Id for the grid
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('salinardi_news_category_edit_form');
    }

    /**
     * _prepareForm
     *
     * Creates the grid in the main section of edit and new pages
     *
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        // prepare form from varien form
        $form = new Varien_Data_Form(
            array(
                'id' => 'edit_form',
                'method' => 'post',
                'action' => $this->getUrl('*/*/save',
                            array('category_id' => $this->getRequest()->getParam('category_id'))),
                'enctype' => 'multipart/form-data',
            )
        );

        // sets html id prefix of fields
        $form->setData('html_id_prefix', 'category_');

        // container of fields
        $fieldset = $form->addFieldset(
            'base_fieldset',
            array(
                'legend' => $this->__('General Information'),
                'class' => 'fieldset-wide',
            )
        );

        /** ***************** FIELDS ********************* */
        $fieldset->addField(
            'code',
            'text',
            array(
                'name' => 'code',
                'label' => $this->__('Code'),
                'title' => $this->__('Code'),
                'required' => true,
            )
        );
        $fieldset->addField(
            'label',
            'text',
            array(
                'name' => 'label',
                'label' => $this->__('Label'),
                'title' => $this->__('Label'),
                'required' => true,
            )
        );

        // gets the model source/status for fill the select field
        /** @var Salinardi_News_Model_Source_Status $statusValues */
        $statusValues = Mage::getModel('salinardi_news/source_status');
        $statusValues = $statusValues->toOptionArray();

        $fieldset->addField(
            'status',
            'select',
            array(
                'name' => 'status',
                'label' => $this->__('Status'),
                'title' => $this->__('Status'),
                'required' => true,
                'values' => $statusValues,
            )
        );



        // gets the category_id parameter passed from the index grid
        $categoryId = $this->getRequest()->getParam('category_id');

        // if category_id is defined, load the object
        if ($categoryId) {

            // gets the row from category table with category_id == $categoryId
            $model = Mage::getModel('salinardi_news/category')->load($categoryId);

            // if model doesn't exist shows the error message
            if (!$model || !$model->getId()) {

                /** @var Mage_Adminhtml_Model_Session $session */
                $session = Mage::getSingleton('adminhtml/session');
                $session->addError($this->__('There was an error when loading the category.
                                            Please, return to the previous page and try again'));
            }

            $form->setValues($model->getData());
        }

        $form->setData('use_container', true);
        $this->setForm($form);
        return parent::_prepareForm();
    }
}
