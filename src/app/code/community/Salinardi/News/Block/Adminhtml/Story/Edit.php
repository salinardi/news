<?php
/**
 * Salinardi_News_Block_Adminhtml_Story_Edit
 */

/**
 * Class Salinardi_News_Block_Adminhtml_Story_Edit
 *
 * Adminhtml Story Edit.
 *
 * @category Salinardi
 * @package News
 *
 * @author Manuel Salinardi <salinardii@libero.it>
 * @version 0.2.0
 * @license GNU General Public License, version 3
 */
class Salinardi_News_Block_Adminhtml_Story_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * Salinardi_News_Block_Adminhtml_Story_Edit constructor.
     */
    public function __construct()
    {
        $this->_objectId = 'story_id';
        $this->_blockGroup = 'salinardi_news';
        $this->_controller = 'adminhtml_story';
        $this->_addButton(
            'saveandcontinue',
            array(
                'label' => $this->__('Save and Continue'),
                'onclick' => 'saveAndContinueEdit()',
                'class' => 'save',
            ),
            '100'
        );
        $this->_formScripts[] = "
		function saveAndContinueEdit() {
			editForm.submit($('edit_form').action + 'back/edit/');
		}
		";
        parent::__construct();
    }

    /**
     * getHeaderText
     * @return string
     */
    public function getHeaderText()
    {
        return $this->__('Story Details');
    }
}
