<?php
/**
 * Salinardi_News_Adminhtml_StoryController
 */

/**
 * Class Salinardi_News_Adminhtml_StoryController
 *
 * Adminhtml controller for menage the stories.
 *
 * @category Salinardi
 * @package News
 *
 * @author Manuel Salinardi <salinardii@libero.it>
 * @version 0.2.0
 * @license GNU General Public License, version 3
 */
class Salinardi_News_Adminhtml_StoryController extends Mage_Adminhtml_Controller_Action
{
    /**
     * indexAction
     *
     * Goes tu http://[domain]/[frontend-name]/index/index
     * @return void
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * newAction
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     * editAction
     */
    public function editAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * saveAction
     * @return $this|Mage_Core_Controller_Varien_Action
     */
    public function saveAction()
    {
        // prepare model
        if ($this->getRequest()->getParam('category_id')) {
            $model = Mage::getModel('salinardi_news/category')
                ->load($this->getRequest()->getParam('category_id'));
            if (!$model || !$model->getId()) {

                /** @var Mage_Adminhtml_Model_Session $session */
                $session = Mage::getSingleton('adminhtml/session');
                $session->addError($this->__('There was an error when loading the category. Please, try again.'));

                return $this->_redirect('*/*/');
            }
        } else {
            $model = Mage::getModel('salinardi_news/category');
        }

        // prepare data to be saved
        $code = $this->getRequest()->getPost('code');
        $label = $this->getRequest()->getPost('label');

        if (!$code || !$label) {

            /** @var Mage_Adminhtml_Model_Session $session */
            $session = Mage::getSingleton('adminhtml/session');
            $session->addError($this->__('A required field was not informed. Please, verify your form.'));

            return $this->_redirect('*/*/');
        }

        // save data
        try {
            $model->setCode($code);
            $model->setLabel($label);
            $model->setStatus($this->getRequest()->getPost('status'));
            $model->save();
            Mage::getSingleton('adminhtml/session')
                ->addSuccess($this->__('The category was successfully saved'));
        } catch (Exception $e) {
            Mage::logException($e);
            Mage::getSingleton('adminhtml/session')
                ->addError($this->__('There was an error when saving the category. Please, try again'));
        }

        // redirect to edit page if Save and Continue was used
        if ($this->getRequest()->getParam('back') && $this->getRequest()->getParam('back') == 'edit') {
            return $this->_redirect('*/*/edit', array('category_id' => $model->getId()));
        }
        return $this->_redirect('*/*/');
    }


    /**
     * deleteAction
     * @return $this|Mage_Core_Controller_Varien_Action
     */
    public function deleteAction()
    {
        // get category ID
        $categoryId = $this->getRequest()->getParam('category_id');
        if (!$categoryId) {
            Mage::getSingleton('adminhtml/session')
                ->addError($this->__('There was an error when deleting this entry. Please, try again'));
            return $this->_redirect('*/*/');
        }

        // load category and delete it
        try {
            $category = Mage::getModel('salinardi_news/category')->load($categoryId);
            $category->delete();
            Mage::getSingleton('adminhtml/session')
                ->addSuccess($this->__('The selected category was successfully deleted'));
        } catch (Exception $e) {
            Mage::logException($e);
            Mage::getSingleton('adminhtml/session')
                ->addError($this->__('There was an error when deleting this entry. Please, try again'));
        }
        return $this->_redirect('*/*/');
    }

    /**
     * _isAllowed
     * @return mixed
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('salinardi_news');
    }

}