<?php
/**
 * Salinardi_News_Adminhtml_CategoryController
 */

/**
 * Class Salinardi_News_Adminhtml_CategoryController
 *
 * Adminhtml controller for menage the categories.
 *
 * @method setCode($code)
 *
 * @category Salinardi
 * @package News
 *
 * @author Manuel Salinardi <salinardii@libero.it>
 * @version 0.2.0
 * @license GNU General Public License, version 3
 */
class Salinardi_News_Adminhtml_CategoryController extends
    Mage_Adminhtml_Controller_Action
{
    /**
     * indexAction
     *
     * Goes tu http://[domain]/[frontend-name]/index/index
     *
     * @return void
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * newAction
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     * editAction
     */
    public function editAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * saveAction
     * @return $this|Mage_Core_Controller_Varien_Action
     */
    public function saveAction()
    {
        // prepare model

        // get id
        $categoryId = $this->getRequest()->getParam('category_id');

        if ($categoryId) {   // se cè il parametro
            $model = Mage::getModel('salinardi_news/category')
                ->load($this->getRequest()->getParam('category_id'));

            // verify if this is a valid object
            if (!$model || !$model->getId()) {

                /** @var Mage_Adminhtml_Model_Session $session */
                $session = Mage::getSingleton('adminhtml/session');
                $session->addError($this->__('There was an error when loading the category.
                                                            Please, try again.'));
                return $this->_redirect('*/*/'); // goes to index page

            }
        } else {
            // if there isn't id category, it is a new, not edit
            $model = Mage::getModel('salinardi_news/category');
        }

        // prepare data to be saved
        $code = $this->getRequest()->getPost('code');
        $label = $this->getRequest()->getPost('label');

        // save data
        try {

            /** @var Salinardi_News_Model_Category $model */
            $model->setCode($code);
            $model->setLabel($label);
            $model->setStatus($this->getRequest()->getPost('status'));
            $model->save();

            /** @var Mage_Adminhtml_Model_Session $session */
            $session = Mage::getSingleton('adminhtml/session');
            $session->addSuccess($this->__('The category was successfully saved'));

        } catch (Exception $e) {
            Mage::logException($e);

            $session = Mage::getSingleton('adminhtml/session');
            $session->addError($this->__('There was an error when saving the category. Please, try again'));
        }

        // redirect to edit page if Save and Continue was used
        if ($this->getRequest()->getParam('back') && $this->getRequest()->getParam('back') == 'edit') {
            return $this->_redirect('*/*/edit', array('category_id' => $model->getId()));
        }
        return $this->_redirect('*/*/');
    }




    /**
     * deleteAction
     * @return $this|Mage_Core_Controller_Varien_Action
     */
    public function deleteAction()
    {
        // get category ID
        $categoryId = $this->getRequest()->getParam('category_id');
        if (!$categoryId) {

            /** @var Mage_Adminhtml_Model_Session $session */
            $session = Mage::getSingleton('adminhtml/session');
            $session->addError($this->__('There was an error when deleting this entry. Please, try again'));

            return $this->_redirect('*/*/');
        }

        // load category and delete it
        try {
            $category = Mage::getModel('salinardi_news/category')->load($categoryId);
            $category->delete();

            /** @var Mage_Adminhtml_Model_Session $session */
            $session = Mage::getSingleton('adminhtml/session');
            $session->addSuccess($this->__('The selected category was successfully deleted'));

        } catch (Exception $e) {
            Mage::logException($e);

            $session = Mage::getSingleton('adminhtml/session');
            $session->addError($this->__('There was an error when deleting this entry. Please, try again'));

        }
        return $this->_redirect('*/*/');
    }

    /**
     * _isAllowed
     * @return mixed
     */
    protected function _isAllowed()
    {
        /** @var Mage_Admin_Model_Session $session */
        $session = Mage::getSingleton('admin/session');
        $session->isAllowed('salinardi_news');
        return $session;
    }

}
