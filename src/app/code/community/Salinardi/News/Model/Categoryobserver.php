<?php

/**
 * Salinardi_News_Model_Categoryobserver
 */

/**
 * Class Salinardi_News_Model_Categoryobserver
 *
 *
 * @category Salinardi
 * @package News
 *
 * @author Manuel Salinardi <salinardii@libero.it>
 * @version 0.2.0
 * @license GNU General Public License, version 3
 */
class Salinardi_News_Model_Categoryobserver extends Mage_Core_Helper_Abstract
{
    public function createLogEntry(Varien_Event_Observer $observer)
    {
        // get category object
        $category = $observer->getData('category');

        if (!$category || !$category->getId()) {
            return $observer;
        }

        Mage::log('category saved. ID: ' . $category->getId());

        return $observer;
    }

}
