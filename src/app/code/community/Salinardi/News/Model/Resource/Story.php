<?php

/**
 * Salinardi_News_Model_Resource_Story
 */

/**
 * Class Salinardi_News_Model_Resource_Story
 *
 * Model.
 *
 * @category Salinardi
 * @package News
 *
 * @author Manuel Salinardi <salinardii@libero.it>
 * @version 0.2.0
 * @license GNU General Public License, version 3
 */
class Salinardi_News_Model_Resource_Story extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * _construct
     *
     *
     */
    public function _construct()
    {
        $this->_init('salinardi_news/story', 'story_id');
    }

}