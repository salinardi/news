<?php
/**
 * Salinardi_News_Model_Source_Status
 */

/**
 * Class Salinardi_News_Model_Source_Status
 *
 * Source Status
 *
 * @category Salinardi
 * @package News
 *
 * @author Manuel Salinardi <salinardii@libero.it>
 * @version 0.2.0
 * @license GNU General Public License, version 3
 */
class Salinardi_News_Model_Source_Status
{
    /**
     * toOptionArray
     *
     * Used to fill dropdown menus.
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => 0, 'label' => Mage::helper('salinardi_news')->__('Unpublished')),
            array('value' => 1, 'label' => Mage::helper('salinardi_news')->__('Published')),
        );
    }

    /**
     * toGridArray
     *
     * Returns toOptionArray method as options to grid column.
     * @return array
     */
    public function toGridArray()
    {
        $array = array();
        foreach ($this->toOptionArray() as $option) {
            $array[$option['value']] = $option['label'];
        }
        return $array;
    }
}
