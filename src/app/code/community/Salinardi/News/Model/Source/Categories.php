<?php
/**
 * Salinardi_News_Block_Adminhtml_Category_Edit_Form
 */

/**
 * Class Salinardi_News_Block_Adminhtml_Category_Edit_Form
 *
 * Source Categories
 *
 * @category Salinardi
 * @package News
 *
 * @author Manuel Salinardi <salinardii@libero.it>
 * @version 0.2.0
 * @license GNU General Public License, version 3
 */
class Salinardi_News_Model_Source_Categories
{
    /**
     * toOptionArray
     *
     * Used to fill dropdown menus.
     * @throws Exception
     * @return array
     */
    public function toOptionArray()
    {
        /** @var Salinardi_News_Model_Category $categories */
        $categories = Mage::getModel('salinardi_news/category')->getCollection();
        if (!$categories) {
            throw new Exception('There was an error when loading the categories for a dropdown menu.');
        }

        // loop categories and build array
        $array = array();
        foreach ($categories as $ca) {
            $array[] = array('value' => $ca->getId(), 'label' => $ca->getLabel());
        }
        return $array;
    }

    /**
     * toGridArray
     *
     * Returns toOptionArray method as options to grid column.
     * @return bool|array
     */
    public function toGridArray()
    {
        // get options
        try {
            $optionsArray = $this->toOptionArray();
        } catch (Exception $e) {
            Mage::logException($e);
            return false;
        }

        if (!$optionsArray) {
            return false;
        }

        // prepare new array
        $array = array();
        foreach ($optionsArray as $option) {
            $array[$option['value']] = $option['label'];
        }

        return $array;
    }
}
