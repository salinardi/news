<?php

/**
 * Salinardi_News_Model_Category
 */

/**
 * class Salinardi_News_Model_Category
 *
 * Main Model.
 *
 * @method string getCode()
 * @method setCode(string $code)
 * @method string getTitle()
 * @method setTitle(string $title)
 * @method boolean getStatus()
 * @method setStatus(string $status)
 * @method setLabel(string $label)
 * @method string getCreatedAt()
 * @method string setUpdateAt()
 *
 * @category Salinardi
 * @package News
 *
 * @author Manuel Salinardi <salinardii@libero.it>
 * @version 0.2.0
 * @license GNU General Public License, version 3
 */
class Salinardi_News_Model_Category extends Mage_Core_Model_Abstract
{
    /**
     * $_eventPrefix
     *
     * Prefix of the name of the event
     *
     * @var string
     */
    protected $_eventPrefix = 'salinardi_news_category';

    /**
     * $_eventObject
     *
     * Name of the event object
     *
     * @var string
     */
    protected $_eventObject = 'category';

    /**
     * _construct
     *
     * init calls the db
     */
    public function _construct()
    {
        $this->_init('salinardi_news/category');
    }

    protected function _beforeSave()
    {
        //return parent::_beforeSave();
        //$this->setUpdateAt(Mage::getSingleton('core/date')->gmtDate());
        //return $this;
    }

}
