<?php

/**
 * Salinardi_News_Model_Story
 */

/**
 * class Salinardi_News_Model_Story
 *
 * Main Model.
 *
 * @method string getCode()
 * @method setCode(string $code)
 * @method string getTitle()
 * @method setTitle(string $title)
 * @method boolean getStatus()
 * @method setStatus(string $status)
 * @method setLabel(string $label)
 * @method string getCreatedAt()
 * @method string setUpdateAt()
 *
 * @category Salinardi
 * @package News
 *
 * @author Manuel Salinardi <salinardii@libero.it>
 * @version 0.2.0
 * @license GNU General Public License, version 3
 */
class Salinardi_News_Model_Story extends Mage_Core_Model_Abstract
{
    /**
     * $_eventPrefix
     * @var string
     */
    protected $_eventPrefix = 'salinardi_news_story'; // prefisso del nome dell'evento

    /**
     * $_eventObject
     * @var string
     */
    protected $_eventObject = 'story'; // nome dell'oggetto evento

    /**
     * _construct
     * init chiama resources che fa le chiamate al db
     */
    public function _construct()
    {
        $this->_init('salinardi_news/story');
    }

    protected function _beforeSave()
    {
        //return parent::_beforeSave();
        //$this->setUpdateAt(Mage::getSingleton('core/date')->gmtDate());
        //return $this;
    }

}
